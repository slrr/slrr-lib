﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SlrrLib.Model.HighLevel
{
  public class RpkMergerRpkRenameEntry
  {
    public string wasRefString;
    public int wasTypeIDLocalPart;
    public string newRefString;
    public int newTypeIDLocalPart;
  }
}
