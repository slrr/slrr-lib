﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SlrrLib.Model.HighLevel
{
  public class RpkMergerResourceDirectoryMoveDescriptor
  {
    public string wasSlrrRelativeDir;
    public string newSlrrRelativeDir;
  }
}
